plugins {
    id("org.jetbrains.kotlin.jvm").version("1.5.0")
    id("com.github.johnrengelman.shadow").version("6.0.0")
    id("org.gradle.maven-publish")
}

group = "com.gitlab.glayve"
version = "1.0.0"

repositories {
    maven("https://repo1.maven.org/maven2")
    maven("https://libraries.minecraft.net")
    mavenLocal(); maven("https://jitpack.io")
}

dependencies {
    api("com.gitlab.ballysta:architecture:1.0.6")
    api("com.github.Exerosis.Mynt:Mynt-jvm:1.0.4")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.0")
    implementation("com.mojang:authlib:1.5.21")
}

tasks.shadowJar {
    archiveFileName.set("${project.name}.jar")
    destinationDirectory.set(file("/Client"))
    manifest.attributes["Main-Class"] = "com.gitlab.glayve.Main"
}

tasks.build { dependsOn(tasks.shadowJar) }

tasks.compileKotlin {
    kotlinOptions.freeCompilerArgs = listOf("-Xinline-classes", "-Xopt-in=kotlin.time.ExperimentalTime")
}