package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.Gamemode.*
import com.gitlab.glayve.network.*
import com.gitlab.glayve.practice.Practice
import kotlinx.coroutines.runBlocking
import java.net.InetSocketAddress
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Instant
import java.util.*
import java.util.concurrent.Executors


val scheduler = Executors.newScheduledThreadPool(2)
val address = InetSocketAddress(25567)
//val address = InetSocketAddress(55898)
const val radius = 2
//val spawn = Location(44.0, 80.0, 325.0, 0f, 0f, false)
val spawn = Location(100.0, 101.0, 233.0, 0f, 0f, false)
//val spawn = Location(-1250.0, 98.0, 950.0, 0f, 0f, false)
var eid = 1

fun chunks(path: Path) = runBlocking {
    ArrayList<Chunk>().apply {
        val centerX = (spawn.x / 16).toInt()
        val centerZ = (spawn.z / 16).toInt()
        for (x in -radius..radius) for (z in -radius..radius)
            loadChunk(path, centerX + x, centerZ + z)?.let(::add)
    }
}

fun Toggled.App() {
    val players = MutableTable<UUID, Player>()
    val connections = LoginComponent(address) {"""{
        "version": { "name": "1.8.8", "protocol": 47 },
        "players": { "max": 20, "online": ${players.size} },
        "description": { "text": "Glayve" }
    }""" }
    val world = HashWorld(LoadedWorld(chunks(Paths.get("r.0.0.mca"))))
    connections.onEach { uuid, channel ->
        channel.joinGame(0, 1, 0, 3, 100, "default", true)

        val id = eid++

        val gamemode = PublishSubject(SURVIVAL)
        val health = PublishSubject(20f)
        val food = PublishSubject(20)
        val player = Player(channel, spawn, id, health, food, gamemode)

        player.onChat { when (it) {
            "/s" -> gamemode(SURVIVAL)
            "/c" -> gamemode(CREATIVE)
            "/a" -> gamemode(ADVENTURE)
            "/h" -> health(20f)
            "/f" -> food(if (food.last == 20) 0 else 20)
            else -> {
                val name = Thread.currentThread().name
                players.send("[$name]$player: $it")
            }
        } }

        //Handle effects and display particles.
        val effects = EffectsComponent(player, players, health)
        player.onChat {
            if (it.startsWith("/effect")) {
                val args = it.split(" ")
                val effect = args.getOrNull(1)?.toIntOrNull()
                if (effect != null) {
                    val amp = args.getOrNull(2)?.toIntOrNull() ?: 0
                    effects[EffectType[effect]] = Effect(20 * 200, amp)
                }
            }
        }



        val inventory = MutableTable<Slot, Item>()
        player.onWindowSlot { slot, item ->
            inventory[slot] = item

            if (slot.toInt() == -1) {
                val itemId = eid++
                val force = dropVelocity(player.location.last)
                val origin = player.location.last.add(y = 1.3199999928474426)

                if (item.type > 0) {
                    EntityDrop(scheduler, itemId, world, players, origin, force, item).enable()
                }
            }
        }

        inventory.onEach { slot, item ->
            channel.setSlot(0, slot.toShort(), item)
        }

        val using = PublishSubject(false).apply {
            player.onUse { item, _ ->
                if (item.isConsumable()) this(true)
            }
            player.onRelease { this(false) }
        }

        player.onDrop {
            val item = inventory[player.selected.last]!!

            if (gamemode.last == SURVIVAL) {
                if (item.count > 1) {
                    println(item.count)
                    inventory[player.selected.last] = item.copy(count = (item.count - 1).toByte())
                } else {
                    inventory[player.selected.last] = Item(0, 0, 0, emptyMap())
                }
            }

            val itemId = eid++
            val force = dropVelocity(player.location.last)
            val origin = player.location.last.add(y = 1.3199999928474426)

            if (item.type > 0) {
                EntityDrop(scheduler, itemId, world, players, origin, force, item.copy(count = 1)).enable()
            }
        }

        InventoryComponent(player, inventory, world, players)


        //Handle changes in health and attacks from other players.
        var next = Instant.now()
        players.onEach { _, attacker -> attacker.onAttack {
            if (it == id && next.isBefore(Instant.now())) {
                next = Instant.now().plusMillis(500)
                val from = attacker.location.last
                var amount = inventory[player.selected.last]?.damage() ?: 1f
                if (!from.ground) amount *= 1.5f
                if (using.last) amount *= 0.5f //FIXME check if they are using a sword.
                if (amount > 0f) health(health.last - amount)

                val velocity = knockbackVelocity(player.location.last, attacker, true, true)
                players.send {
                    velocity(if (this !== channel) id else 0, velocity)
                    if (!from.ground) animation(id, 4) //do we need to do self id?
                }
            }
        } }

        health { if (it <= 0) {
            health(20f)
            player.channel.teleport(spawn)
            players.send("${player.name} has died!")
        } }

        val breaking = WorldComponent(player, world) {
            if (gamemode.last != CREATIVE) when (world[it]) {
                288 -> 6; 296 -> 6; 48 -> 14; 32 -> 18
                272 -> 59; 16 -> 150; else -> -1
            } else -1
        }
        players.onEach { _, other ->
            if (other !== player) breaking.streamTo(other.channel)
        }

        //Add or remove the player in a stupid way and send their join or quit message.
        onEnabled { players[uuid] = player; players.send("$player has joined the game!", "yellow") }
        onDisabled { players[uuid] = null; players.send("$player has left the game!", "yellow") }

        //Player list stuff.
        players.onEach { _, other ->
            val item = PlayerListItem(player.uuid, player.name, player.properties)
            onEnabled { other.channel.playerListItems(0, item) }
            if (other !== player) EntityPlayer(
                other.channel, id, player.uuid, player.location,
                health, inventory.watch(player.selected),
                inventory.watch(5), inventory.watch(6),
                inventory.watch(7), inventory.watch(8),
                player.sprinting, player.sneaking,
                just(false), using, player.onSwing
            )
            onDisabled { other.channel.playerListItems(4, item) }
        }

        ThrowableComponent(player, players, scheduler, world, health)
        ConsumableComponent(player, players, scheduler)
      //  Practice(players)
    }



    onEnabled { println("[Glayve] Enabled"); }
    onDisabled { println("[Glayve] Disabled") }
}

fun main() = Component { App() }.enable()