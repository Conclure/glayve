@file:Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")
package com.gitlab.glayve.network

import com.gitlab.mynt.base.Read
import com.gitlab.mynt.base.Write
import com.gitlab.mynt.bytes
import java.nio.charset.StandardCharsets.UTF_8
import java.util.*
import kotlin.math.max

const val END = 0x0.toByte()
const val BYTE = 0x1.toByte()
const val SHORT = 0x2.toByte()
const val INT = 0x3.toByte()
const val LONG = 0x4.toByte()
const val FLOAT = 0x5.toByte()
const val DOUBLE = 0x6.toByte()
const val BYTES = 0x7.toByte()
const val STRING = 0x8.toByte()
const val OBJECTS = 0x9.toByte()
const val OBJECT = 0xA.toByte()
const val INTS = 0xB.toByte()
const val LONGS = 0xC.toByte()

typealias Tag = Any
typealias TagCompound = Map<String, Tag>
typealias TagList = Array<Tag>
fun Tag.id() = when (this) {
    is Byte -> BYTE
    is Short -> SHORT
    is Int -> INT
    is Long -> LONG
    is Float -> FLOAT
    is Double -> DOUBLE
    is ByteArray -> BYTES
    is String -> STRING
    is Array<*> -> OBJECTS
    is Map<*, *> -> OBJECT
    is IntArray -> INTS
    is LongArray -> LONGS
    else -> throw IllegalStateException("Invalid NBT Tag Type")
}

suspend fun Read.tag(type: Byte): Tag {
    return when (type) {
        BYTE -> byte()
        SHORT -> short()
        INT -> int()
        LONG -> long()
        FLOAT -> float()
        DOUBLE -> double()
        BYTES -> ByteArray(int()) { byte() }
        STRING -> ByteArray(short().toInt()) { byte() }.toString(UTF_8)
        OBJECTS -> {
            val id = byte()
            Array(max(0, int())) { tag(id) }
        }
        OBJECT -> HashMap<String, Any>().apply {
            var id = byte()
            while (id != END) {
                val name = ByteArray(short().toInt()) { byte() }
                put(name.toString(UTF_8), tag(id)); id = byte()
            }
        }
        INTS -> IntArray(int()) { int() }
        LONGS -> LongArray(int()) { long() }
        else -> throw IllegalStateException("Invalid NBT Tag Type")
    }
}
suspend fun Write.tag(tag: Tag, id: Byte = tag.id()) {
    when (id) {
        BYTE -> byte(tag as Byte)
        SHORT -> short(tag as Short)
        INT -> int(tag as Int)
        LONG -> long(tag as Long)
        FLOAT -> float(tag as Float)
        DOUBLE -> double(tag as Double)
        BYTES -> { int((tag as ByteArray).size); bytes(tag) }
        STRING -> { short((tag as String).length.toShort()); bytes(tag.toByteArray(UTF_8)) }
        OBJECTS -> (tag as TagList).apply {
            val type = tag.getOrNull(0)?.id() ?: BYTE
            byte(type); int(tag.size); forEach { tag(it, type) }
        }
        OBJECT -> (tag as TagCompound).apply {
            forEach { (name, value) ->
                byte(value.id()); tag(name, STRING); tag(value)
            }; byte(0)
        }
        else -> throw IllegalStateException("Invalid NBT Tag Type")
    }
}

suspend fun Read.compound(): TagCompound {
    if (byte() != OBJECT) throw IllegalStateException("Corrupted NBT.")
    skip(short().toInt())
    val tag = tag(OBJECT)
    return tag as Map<String, Any>
}
suspend fun Write.compound(compound: TagCompound) {
    byte(OBJECT); short(0); tag(compound, OBJECT)
}