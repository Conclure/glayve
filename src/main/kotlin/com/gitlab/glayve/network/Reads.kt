package com.gitlab.glayve.network

import com.gitlab.mynt.base.Read
import java.io.DataInputStream
import java.nio.ByteBuffer
import java.nio.channels.SeekableByteChannel

fun SeekableByteChannel.toRead() = object : Read {
    override suspend fun skip(
        amount: Int
    ) { position(this@toRead.position() + amount) }
    override suspend fun buffer(
        buffer: ByteBuffer,
        amount: Int
    ) = buffer.apply {
        limit(buffer.position() + amount)
        read(buffer)
    }
    override suspend fun bytes(
        bytes: ByteArray,
        amount: Int,
        offset: Int
    ) = buffer(ByteBuffer.wrap(bytes).position(offset) as ByteBuffer).array()

    override suspend fun byte() =  buffer(ByteBuffer.allocate(1), 1).get(0)
    override suspend fun short() = buffer(ByteBuffer.allocate(2), 2).getShort(0)
    override suspend fun int() = buffer(ByteBuffer.allocate(4), 4).getInt(0)
    override suspend fun float() = buffer(ByteBuffer.allocate(4), 4).getFloat(0)
    override suspend fun long() = buffer(ByteBuffer.allocate(8), 8).getLong(0)
    override suspend fun double() = buffer(ByteBuffer.allocate(8), 8).getDouble(0)
}
fun DataInputStream.toRead() = object : Read {
    override suspend fun skip(amount: Int) {
        skipBytes(amount)
    }
    override suspend fun buffer(buffer: ByteBuffer, amount: Int): ByteBuffer {
        read(buffer.array(), buffer.position(), amount)
        return buffer
    }
    override suspend fun bytes(bytes: ByteArray, amount: Int, offset: Int): ByteArray {
        var required = amount
        while (required > 0) required -= read(bytes, offset + (amount - required), amount)
        return bytes
    }

    override suspend fun byte() = readByte()
    override suspend fun short() = readShort()
    override suspend fun int() = readInt()
    override suspend fun float() = readFloat()
    override suspend fun long() = readLong()
    override suspend fun double() = readDouble()
}
inline fun ByteBuffer.toRead() = object : Read {
    override suspend fun skip(amount: Int) {
        position(this@toRead.position() + amount)
    }
    override suspend fun buffer(buffer: ByteBuffer, amount: Int) = buffer.also {
        buffer.limit(buffer.position() + amount)
        put(buffer)
    }
    override suspend fun bytes(bytes: ByteArray, amount: Int, offset: Int) = bytes.also {
        put(bytes, offset, amount)
    }

    override suspend fun byte() = get()
    override suspend fun short() = short
    override suspend fun int() = int
    override suspend fun float() = float
    override suspend fun long() = long
    override suspend fun double() = double
}