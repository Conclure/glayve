@file:Suppress("BlockingMethodInNonBlockingContext")
package com.gitlab.glayve.network

import com.gitlab.ballysta.architecture.*
import com.gitlab.mynt.TCPSocketProvider
import com.gitlab.mynt.base.Connection
import com.gitlab.mynt.base.Read
import com.gitlab.mynt.base.Write
import com.gitlab.mynt.bytes
import com.mojang.authlib.GameProfile
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.io.IOException
import java.net.Proxy.*
import java.net.SocketAddress
import java.nio.ByteBuffer
import java.nio.ByteBuffer.*
import java.nio.channels.AsynchronousChannelGroup.withThreadPool
import java.nio.charset.StandardCharsets.UTF_8
import java.time.Instant.now
import java.util.*
import java.util.UUID.*
import java.util.concurrent.*
import java.util.concurrent.ThreadLocalRandom.current
import javax.crypto.Cipher.*

suspend inline fun <Return> Read.packet(id: Int, block: Read.() -> Return): Return {
    varInt() //Packet length. (Ignored)
    if (varInt() == id) return block()
    throw IllegalStateException("No longer in sync, aborting!")
}
suspend inline fun <Return> Write.packet(id: Int, block: Write.() -> Return): Return {
    val buffer = ByteArrayOutputStream()
    val out = DataOutputStream(buffer)
    val write = object : Write {
        override suspend fun buffer(
            buffer: ByteBuffer,
            amount: Int
        ) = out.write(buffer.array(), buffer.position(), amount)

        override suspend fun bytes(
            bytes: ByteArray,
            amount: Int,
            offset: Int
        ) = out.write(bytes, offset, amount)

        override suspend fun byte(byte: Byte) = out.writeByte(byte.toInt())
        override suspend fun short(short: Short) = out.writeShort(short.toInt())
        override suspend fun int(int: Int) = out.writeInt(int)
        override suspend fun float(float: Float) = out.writeFloat(float)
        override suspend fun long(long: Long) = out.writeLong(long)
        override suspend fun double(double: Double) = out.writeDouble(double)
    }
    write.varInt(id)
    val result = block(write)
    val bytes = buffer.toByteArray()
    varInt(bytes.size)
    bytes(bytes, bytes.size, 0)
    return result
}


interface Channel {
    val profile: GameProfile
    val name get() = profile.name!!
    fun receive(id: Int, packet: suspend (Read).(Int) -> (Unit))
    fun send(id: Int, packet: suspend (Write).() -> (Unit))
}

fun Toggled.LoginComponent(
    address: SocketAddress, info: () -> (String)
): Table<UUID, Channel> {
    val table = MutableTable<UUID, Channel>()
    val authentication = YggdrasilAuthenticationService(NO_PROXY, randomUUID().toString())
    val sessions = authentication.createMinecraftSessionService()
    val executor = Executors.newCachedThreadPool()
    val dispatcher = executor.asCoroutineDispatcher()
    val allocator = { allocateDirect(1048576).flip() as ByteBuffer }
    val provider = TCPSocketProvider(withThreadPool(executor), allocator)
    val (public, private) = createKeys("RSA")
    val decryption = RSA(DECRYPT_MODE, private)
    val server = "".toByteArray(UTF_8)

    suspend fun Connection.handle() {
        read.varInt() //Handshake length. (Ignored)
        read.varInt() //Handshake id. (Ignored)
        val version = read.varInt()
        val hostname = read.string()
        val port = read.short()
        if (read.varInt() == 1) read.packet(0x00) {
            write.packet(0x00) { string(info()) }
            val time = read.packet(0x01) { long() }
            write.packet(0x01) { long(time) }
        } else {
            val username = read.packet(0x00) { string() }
            println("[Glayve] ${username}@$hostname:$port - $version is trying to connect.")
            val tokens = ByteArray(4).also(current()::nextBytes)
            write.packet(0x01) {
                varInt(server.size); bytes(server)
                varInt(public.size); bytes(public)
                varInt(tokens.size); bytes(tokens)
            }
            val secret = read.packet(0x01) {
                val secret = decryption.doFinal(bytes(varInt()))
                val verify = decryption.doFinal(bytes(varInt()))
                if (tokens.contentEquals(verify)) secret
                else error("Invalid authentication token.")
            }

            val profile = sessions.hasJoinedServer(
                GameProfile(null, username), createHexdigest(secret, public)
            ) ?: return write.packet(0x00) { string("Failed to login.") }
            val decrypted = decrypt(read, AES(DECRYPT_MODE, secret))
            val encrypted = encrypt(write, AES(ENCRYPT_MODE, secret))
            try {
                val player = object : Channel {
                    val inbound = Array<suspend (Read).(Int) -> (Unit)>(256) { { skip(it) } }
                    val outbound = ConcurrentLinkedDeque<suspend () -> (Unit)>()
                    var reading = false

                    override val profile = profile
                    override fun receive(
                        id: Int, packet: suspend (Read).(Int) -> (Unit)
                    ) { inbound[id] = packet }
                    override fun send(
                        id: Int, packet: suspend (Write).() -> (Unit)
                    ) {
                        synchronized(outbound) {
                            if (!reading) {
                                reading = true
                                GlobalScope.launch(dispatcher) {
                                    encrypted.packet(id) { packet() }
                                    val next = outbound.poll()
                                    if (next != null) next()
                                    else reading = false
                                }
                            } else if (outbound.size > 1000000) outbound.addFirst {
                                println("connection unsuitable for online play: ${outbound.size}")
                                encrypted.packet(OUT_DISCONNECT) {
                                    string("{\"text\":\"connection unsuitable for online play\"}")
                                }
                                close()
                            } else outbound.addLast {
                                encrypted.packet(id) { packet() }
                                val next = outbound.poll()
                                if (next != null) next()
                                else reading = false
                            }
                        }
                    }
                }
                encrypted.packet(0x02) { string(profile.id.toString()); string(profile.name) }

                table[profile.id] = player
                var last = now()
                player.receive(0x00) {
                    varInt()
                    last = now()
                }

                GlobalScope.launch(dispatcher) {
                        while (isOpen) {
                            player.send(0x00) { varInt(current().nextInt()) }
                            delay(10_000)
//                            if (last.isBefore(now().plusMillis(-20_000))) {
//                                println("is this where the removal happens?")
//                                player.send(OUT_DISCONNECT) { string("{\"text\":\"Timed out!\"}") }
//                                table[profile.id] = null
//                        }
                    }
                }

                while (isOpen) {
                    val length = decrypted.varInt()
                    val id = decrypted.varInt()
                    val offset = when {
                        id < 127 -> 1
                        id < 16383 -> 2
                        id < 2097151 -> 3
                        id < 268435455 -> 4
                        else -> 5
                    }
                    val buffer = wrap(decrypted.bytes(length - offset))
                    player.inbound[id](buffer.toRead(), buffer.remaining())
                    if (buffer.hasRemaining()) error("did not completely read out packet: $id")
                    //                if (id == 0x00) {
                    //                    decrypted.varInt() //Keep alive id.
                    //                    last = now()
                    //                } else {
                    //
                    //                }
                }
            } catch (reason: Throwable) {
                if (reason !is IOException)
                    reason.printStackTrace()
                table[profile.id] = null
                close()
            }
        }
    }
    var task: Job? = null
    onEnabled {
        task = GlobalScope.launch(dispatcher) {
            while (provider.isOpen) {
                val connection = provider.accept(address)
                launch {
                    try {
                        connection.handle()
                    } catch (reason: Throwable) {
                        reason.printStackTrace()
                    }
                }
            }
        }
    }
    onDisabled { task?.cancel("Server closed") }
    return table
}