package com.gitlab.glayve.network

import com.gitlab.glayve.*
import com.mojang.authlib.properties.Property
import java.util.*

const val OUT_JOIN_GAME = 0x01
const val OUT_TIME_UPDATE = 0x03
const val OUT_PLAYER_POSITION_AND_LOOK = 0x08
const val OUT_CHUNK_BULK = 0x26
const val OUT_PLAYER_LIST_ITEMS = 0x38
const val OUT_SPAWN_PLAYER = 0x0C
const val OUT_DISCONNECT = 0x40
const val OUT_CHAT = 0x02
const val OUT_GAME_STATE = 0x2B
const val OUT_ABILITIES = 0x39
const val OUT_BLOCK_CHANGE = 0x23
const val OUT_VELOCITY = 0x12
const val OUT_REMOVE_ENTITIES = 0x13
const val OUT_MOVE_HEAD = 0x19
const val OUT_SOUND_EFFECT = 0x29
const val OUT_PARTICLE_EFFECT = 0x2A
const val OUT_EFFECT = 0x28
const val OUT_ADD_EFFECT = 0x1D
const val OUT_REMOVE_EFFECT = 0x1E
const val OUT_ENCHANT = 0x11
const val OUT_SET_SLOT = 0x2F
const val OUT_METADATA = 0x1C
const val OUT_STATUS = 0x1A
const val OUT_RESPAWN = 0x07

data class PlayerListItem(
    val uuid: UUID,
    val name: String? = null,
    val properties: Array<Property> = emptyArray(),
    val gamemode: Int = -1,
    val ping: Int = -1,
    val displayName: String? = null
)
data class Meta(val type: Int, val value: Any)

fun Channel.joinGame(
    eid: Int, gamemode: Byte, dimension: Byte,
    difficulty: Byte, maxPlayers: Byte,
    levelType: String, verbose: Bool
) = send(OUT_JOIN_GAME) {
    int(eid); byte(gamemode); byte(dimension)
    byte(difficulty); byte(maxPlayers)
    string(levelType); bool(!verbose)
}
fun Channel.teleport(
    location: Location
) = send(OUT_PLAYER_POSITION_AND_LOOK) {
    double(location.x); double(location.y); double(location.z)
    float(location.yaw); float(location.pitch); byte(0)
}
fun Channel.timeUpdate(
    time: Long, cycle: Boolean
) = send(OUT_TIME_UPDATE) {
    long(time)
    val day = time % 24000
    long(when {
        cycle -> day
        day == 0L -> -1L
        else -> -time
    })
}
fun Channel.chunkBulk(
    chunks: Array<Chunk>, sky: Boolean
) = send(OUT_CHUNK_BULK) {
    bool(sky); varInt(chunks.size)
    chunks.forEach { chunk ->
        val selected = chunk.sections.foldIndexed(0) { i, selected, section ->
            selected or if (section.isEmpty) 0 else (1 shl i)
        }
        int(chunk.x); int(chunk.z); short(selected.toShort())
    }
    chunks.forEach { chunk ->
        chunk.sections.forEach {
            if (!it.isEmpty) it.blockData.forEach { block ->
                byte(block.toByte()); byte((block.toInt() ushr 8).toByte())
            }
        }
        chunk.sections.forEach { if (!it.isEmpty) bytes(it.emittedLight, it.emittedLight.size, 0) }
        if (sky) chunk.sections.forEach { if (!it.isEmpty) bytes(it.skyLight, it.skyLight.size, 0) }
        bytes(chunk.biomes, chunk.biomes.size, 0)
    }
}
fun Channel.blockChange(
    position: Position, type: Int
) = send(OUT_BLOCK_CHANGE) {
    position(position); varInt(type)
}
fun Channel.playerListItems(
    action: Int, vararg items: PlayerListItem
) = send(OUT_PLAYER_LIST_ITEMS) {
    varInt(action); varInt(items.size)
    items.forEach { item ->
        uuid(item.uuid)
        if (action == 0) {
            string(item.name!!)
            varInt(item.properties.size)
            item.properties.forEach {
                string(it.name)
                string(it.value)
                val signed = it.signature != null
                bool(signed);
                if (signed) string(it.signature!!)
            }
        }
        if (action == 0 || action == 1)
            varInt(item.gamemode)
        if (action == 0 || action == 2)
            varInt(item.ping)
        if (action == 0 || action == 3) {
            val hasDisplayName = item.displayName != null
            bool(hasDisplayName)
            if (hasDisplayName) string(item.displayName!!)
        }
    }
}
fun Channel.spawn(
    id: Int, uuid: UUID, location: Location, item: Short, meta: Map<Int, Meta>
) = send(OUT_SPAWN_PLAYER) {
    varInt(id)
    uuid(uuid)
    int((location.x * 32).toInt())
    int((location.y * 32).toInt())
    int((location.z * 32).toInt())
    byte(location.compressedYaw)
    byte(location.compressedPitch)
    short(item)
    meta.forEach { (id, meta) ->
        byte((id or (meta.type shl 5)).toByte())
        when (meta.type) {
            0 -> byte(meta.value as Byte)
            1 -> short(meta.value as Short)
            2 -> int(meta.value as Int)
            3 -> float(meta.value as Float)
            4 -> string(meta.value as String)
            5 -> item(meta.value as Item)
            6 -> TODO("implement x,y,z")
            7 -> TODO("implement yaw,pitch,roll")
        }
    }
    byte(127.toByte())
}
fun Channel.entityMove(
    entity: Int, from: Location,
    to: Location, sync: Bool = true
) = when {
    from == to -> send(0x14) { varInt(entity) }
    from.distance(to) < 4 && !sync -> {
        if (from.yaw != to.yaw || from.pitch != to.pitch) send(0x17) {
            varInt(entity)
            byte(((to.x - from.x) * 32).toInt().toByte())
            byte(((to.y - from.y) * 32).toInt().toByte())
            byte(((to.z - from.z) * 32).toInt().toByte())
            byte(to.compressedYaw); byte(to.compressedPitch)
            bool(to.ground)
        } else send(0x15) {
            varInt(entity)
            byte(((to.x - from.x) * 32).toInt().toByte())
            byte(((to.y - from.y) * 32).toInt().toByte())
            byte(((to.z - from.z) * 32).toInt().toByte())
            bool(to.ground)
        }
    }
    else -> send(0x18) {
        varInt(entity)
        int((to.x * 32).toInt()); int((to.y * 32).toInt())
        int((to.z * 32).toInt()); byte(to.compressedYaw)
        byte(to.compressedPitch); bool(to.ground)
    }
}
fun Channel.despawn(
    vararg entities: Int
) = send(OUT_REMOVE_ENTITIES) {
    varInt(entities.size)
    entities.forEach { varInt(it) }
}
fun Channel.headMove(
    entity: Int, yaw: Float
) = send(OUT_MOVE_HEAD) {
    varInt(entity)
    byte((yaw * 256 / 360).toInt().toByte())
}
fun Channel.updateHealth(
    health: Float, food: Int, saturation: Float
) = send(0x06) {
    float(health); varInt(food); float(saturation)
}
fun Channel.animation(
    entity: Int, animation: Int
) = send(0x0B) {
    varInt(entity); byte(animation.toByte())
}
fun Channel.metadata(
    entity: Int, meta: Map<Int, Meta>
) = send(OUT_METADATA) {
    varInt(entity)
    meta.forEach { (id, meta) ->
        byte((id or (meta.type shl 5)).toByte())
        when (meta.type) {
            0 -> byte(meta.value as Byte)
            1 -> short(meta.value as Short)
            2 -> int(meta.value as Int)
            3 -> float(meta.value as Float)
            4 -> string(meta.value as String)
            5 -> item(meta.value as Item)
            6 -> TODO("implement x,y,z")
            7 -> TODO("implement yaw,pitch,roll")
        }
    }
    byte(127.toByte())
}
fun Channel.equipment(
    entity: Int, slot: Short, item: Item
) = send(0x04) {
    varInt(entity); short(slot); item(item)
}
fun Channel.velocity(
    entity: Int, velocity: Velocity
) = send(OUT_VELOCITY) {
    varInt(entity)
    short(velocity.compressedX)
    short(velocity.compressedY)
    short(velocity.compressedZ)
}
fun Channel.sound(
    key: String, location: Location, volume: Float, pitch: Float
) = send(OUT_SOUND_EFFECT) {
    string(key)
    int((location.x * 8.0).toInt())
    int((location.y * 8.0).toInt())
    int((location.z * 8.0).toInt())
    float(volume * 100f)
    byte((pitch * 63f).toInt().toByte())
}
fun Channel.particle(
    id: Int, distance: Boolean,
    x: Float, y: Float, z: Float,
    offsetX: Float, offsetY: Float,
    offsetZ: Float, speed: Float,
    count: Int, vararg data: Int
) = send(OUT_PARTICLE_EFFECT) {
    int(id)
    bool(distance)
    float(x)
    float(y)
    float(z)
    float(offsetX)
    float(offsetY)
    float(offsetZ)
    float(speed)
    int(count)

    data.forEach { varInt(it) }
}
fun Channel.effect(id: Int, position: Position, data: Int, relative: Bool) = send(OUT_EFFECT) {
    int(id)
    position(position)
    int(data)
    bool(relative)
}
fun Channel.breakAnimation(
    entity: Int, position: Position, stage: Int
) = send(0x25) {
    varInt(entity); position(position); byte(stage.toByte())
}
fun Channel.spawnObject(
    entity: Int, type: Byte, location: Location,
    data: Int, velocity: Velocity
) = send(0x0E) {
    varInt(entity); byte(type)
    int((location.x * 32).toInt())
    int((location.y * 32).toInt())
    int((location.z * 32).toInt())
    byte(location.compressedPitch)
    byte(location.compressedYaw)
    int(data)
    short((velocity.x * 8000).toInt().toShort())
    short((velocity.y * 8000).toInt().toShort())
    short((velocity.z * 8000).toInt().toShort())
}
fun Channel.addEffect(entity: Int, type: EffectType, effect: Effect, particles: Bool = false) = send(OUT_ADD_EFFECT) {
    varInt(entity)
    byte((type.ordinal + 1).toByte())
    byte(effect.strength.toByte())
    varInt(effect.duration.toLongTicks().toInt())
    bool(particles)
}
fun Channel.removeEffect(entity: Int, type: EffectType) = send(OUT_REMOVE_EFFECT) {
    varInt(entity)
    byte((type.ordinal + 1).toByte())
}
fun Channel.enchant(id: Byte, enchant: Byte) = send(OUT_ENCHANT) {
    byte(id)
    byte(enchant)
}
fun Channel.setSlot(id: Byte, slot: Slot, item: Item) = send(OUT_SET_SLOT) {
    byte(id); slot(slot); item(item)
}
fun Channel.spawnEntity(
    id: Int, type: Byte, location: Location,
    velocity: Velocity, meta: Map<Int, Meta>
) = send(0x0F) {
    varInt(id); byte(type)
    int((location.x * 32).toInt())
    int((location.y * 32).toInt())
    int((location.z * 32).toInt())
    byte(location.compressedYaw)
    byte(location.compressedPitch)
    byte(location.compressedYaw)
    short(velocity.compressedX)
    short(velocity.compressedY)
    short(velocity.compressedZ)
    meta.forEach { (id, meta) ->
        byte((id or (meta.type shl 5)).toByte())
        when (meta.type) {
            0 -> byte(meta.value as Byte)
            1 -> short(meta.value as Short)
            2 -> int(meta.value as Int)
            3 -> float(meta.value as Float)
            4 -> string(meta.value as String)
            5 -> item(meta.value as Item)
            6 -> TODO("implement x,y,z")
            7 -> TODO("implement yaw,pitch,roll")
        }
    }
}
fun Channel.state(reason: Byte, value: Float) =
    send(OUT_GAME_STATE) { byte(reason); float(value) }
fun Channel.chat(message: Any, color: Any, action: Bool) = send(OUT_CHAT) {
    string("""{"color": "$color","text": "$message"}""")
    byte(if (action) 2 else 0)
}

class Modifier(val id: UUID, val amount: Double, val operation: Int)
class Attribute(val name: String, val value: Double, vararg val modifiers: Modifier)

fun Channel.attributes(entity: Int, vararg attributes: Attribute) = send(0x20) {
    varInt(entity)
    int(attributes.size)
    attributes.forEach {
        string(it.name)
        double(it.value)
        varInt(it.modifiers.size)
        it.modifiers.forEach { modifier ->
            uuid(modifier.id)
            double(modifier.amount)
            byte(modifier.operation.toByte())
        }
    }
}

fun Channel.status(id: Int, status: Byte) = send(OUT_STATUS) {
    int(id)
    byte(status)
}

fun Channel.respawn(dimension: Int, difficulty: Byte, gamemode: Byte, level: String) = send(OUT_RESPAWN) {
    int(dimension)
    byte(difficulty)
    byte(gamemode)
    string(level)
}

fun Channel.collect(collected: Int, collector: Int) = send(0x0D) {
    varInt(collected)
    varInt(collector)
}
//data class Slot(val present: Boolean, val item: OptionalInt, val count: Optional<Byte>)