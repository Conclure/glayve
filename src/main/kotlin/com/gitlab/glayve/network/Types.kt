@file:Suppress(
    "NOTHING_TO_INLINE", "UNCHECKED_CAST",
    "BlockingMethodInNonBlockingContext",
    "IMPLICIT_NOTHING_TYPE_ARGUMENT_AGAINST_NOT_NOTHING_EXPECTED_TYPE"
)
package com.gitlab.glayve.network

import com.gitlab.glayve.Location
import com.gitlab.mynt.base.Read
import com.gitlab.mynt.base.Write
import com.gitlab.mynt.bytes
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.experimental.and

//--Bool--
typealias Bool = Boolean
const val ERR_BOOL = "Corrupted boolean.";
suspend fun Read.bool(): Bool {
    return when (byte().toInt()) {
        0x00 -> false
        0x01 -> true
        else -> throw IllegalStateException(ERR_BOOL)
    }
}
suspend inline fun Write.bool(bool: Bool) {
    byte(if (bool) 1 else 0)
}

//--String--
const val ERR_STRING = "Failed to read corrupted string."
suspend fun Read.string(): String {
    val length = varInt()
    if (length !in 0..32767) throw IllegalStateException(ERR_STRING)
    return bytes(length).toString(StandardCharsets.UTF_8)
}
suspend fun Write.string(string: String) {
    if (string.length > 32767) throw IllegalStateException(ERR_STRING)
    val bytes = string.toByteArray(StandardCharsets.UTF_8)
    varInt(bytes.size); bytes(bytes)
}

//--VarInt--
typealias VarInt = Int
const val ERR_VAR_INT = "Corrupted variable length integer.";
suspend fun Read.varInt(offset: Int = 0): VarInt {
    if (offset > 36) throw IllegalStateException(ERR_VAR_INT)
    val value = byte().toInt(); val part = value and 127
    return part shl offset or (if (value == part) 0 else varInt(offset + 7))
}
tailrec suspend fun Write.varInt(varInt: VarInt, offset: Int = 0) {
    if (offset > 36) throw IllegalStateException(ERR_VAR_INT)
    val value = varInt ushr offset
    if (value < 128) return byte((value and 127).toByte())
    byte((value or 128).toByte()); varInt(varInt, offset + 7)
}

//--VarLong--
typealias VarLong = Long
const val ERR_VAR_LONG = "Corrupted variable length long.";
suspend fun Read.varLong(offset: Int = 0): VarLong {
    if (offset > 56) throw IllegalStateException(ERR_VAR_LONG)
    val value = byte().toLong(); val part = value and 127L
    return part shl offset or (if (value == part) 0 else varLong(offset + 7))
}
tailrec suspend fun Write.varLong(varLong: VarLong, offset: Int = 0) {
    if (offset > 56) throw IllegalStateException(ERR_VAR_LONG)
    val value = varLong ushr offset
    if (value < 128) return byte((value and 127).toByte())
    byte((value or 128).toByte()); varLong(varLong, offset + 7)
}

//--UUID--
suspend fun Read.uuid() = UUID(long(), long())
suspend fun Write.uuid(uuid: UUID) {
    long(uuid.mostSignificantBits)
    long(uuid.leastSignificantBits)
}

//--Position--
inline class Position(val value: Long) {
    constructor(x: Int, y: Int, z: Int) : this(
        (x.toLong() shl 38) or
        (y.toLong() shl 26) or
        (z.toLong() and 0x3FFFFFF)
    )
    val x get(): Int = (value shr 38).toInt()
    val y get(): Int = ((value shr 26) and 0xFFF).toInt()
    val z get(): Int = ((value shl 38) shr 38).toInt()

    fun relative(face: Face) = Position(
        x + face.x, y + face.y, z + face.z
    )

    fun toLocation() = Location(x.toDouble(), y.toDouble(), z.toDouble(), 0f, 0f, false)

    fun copy(x: Int = this.x, y: Int = this.y, z: Int = this.z) = Position(x, y, z)
    override fun toString() = "X: $x Y: $y Z: $z"
}
suspend inline fun Read.position(): Position {
    return Position(long())
}
suspend inline fun Write.position(position: Position) {
    long(position.value)
}

//--Face--
enum class Face(
    val x: Int, val y: Int, val z: Int
) {
    BOTTOM(0, -1, 0), TOP(0, 1, 0),
    NORTH(0, 0, -1), SOUTH(0, 0, 1),
    WEST(-1, 0, 0), EAST(1, 0, 0)
}
suspend inline fun Read.face(): Face {
    return Face.values()[byte().toInt()]
}
suspend inline fun Write.face(face: Face) {
    byte(face.ordinal.toByte())
}

//--Item--
data class Item(
    val type: Short, val meta: Short,
    val count: Byte, val tags: Map<String, Any>
) {
    companion object { val EMPTY = Item(0, 0, 0, emptyMap()) }
    val combinedType = (type.toInt() shl 4) or meta.toInt()
    override fun toString() = "Type: $type Meta: $meta Count: $count"
}

fun Item.enchant(id: Short, level: Short): Item {
    val enchantments = (tags["ench"] as Array<MutableMap<String, Short>>?) ?: emptyArray()
    for (enchantment in enchantments)
        if (enchantment["id"] == id) {
            enchantment["level"] = level
            return copy(tags = tags + ("ench" to enchantments));
        }

    return copy(tags = tags + ("ench" to enchantments + mutableMapOf("id" to id, "lvl" to level)))
}

suspend fun Read.item(): Item {
    val id = short()
    if (id < 1) return Item(0, 0, 0, emptyMap())
    return Item(
        type = id, count = byte(), meta = short(), tags
        = if (byte() > 0) compound() else emptyMap()
    )
}
suspend fun Write.item(item: Item) {
    if (item.type > 0 && item.count > 0) {
        short(item.type); byte(item.count); short(item.meta)
        if (item.tags.isEmpty()) byte(0) else compound(item.tags)
    } else short(-1)
}

//--Slot--
typealias Slot = Byte
suspend fun Read.slot(): Slot {
    return short().toByte()
}
suspend fun Write.slot(slot: Slot) {
    short(slot.toShort())
}
//
////--TagCompound--
//suspend fun Read.tags(): TagCompound? {
//    val length = short().toInt()
//    return if (length < 1) null
//    else withContext(Dispatchers.IO) {
//        DataInputStream(GZIPInputStream(bytes(length).inputStream())).use {
//            if (it.available() < 1) null else Tag.read(it, Tag.HEAD)
//        }
//    }
//}
//suspend fun Write.tags(tags: TagCompound?) {
//    if (tags == null) { short(-1); return }
//    val bytes = ByteArrayOutputStream()
//    withContext(Dispatchers.IO) {
//        DataOutputStream(GZIPOutputStream(bytes)).use {
//            Tag.write(it, Tag.HEAD, tags.value());
//        }
//    }
//    short(bytes.size().toShort())
//    bytes(bytes.toByteArray())
//}
//

