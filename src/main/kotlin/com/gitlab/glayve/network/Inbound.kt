package com.gitlab.glayve.network

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.Location
import com.gitlab.glayve.Player
import com.gitlab.glayve.isConsumable
import com.gitlab.mynt.base.Read
import kotlin.experimental.and
import kotlin.experimental.or

const val IN_CHAT = 0x01 //1
const val IN_INTERACT = 0x02 //2
const val IN_PLAYER = 0x03 //3
const val IN_PLAYER_POSITION = 0x04 //4
const val IN_PLAYER_LOOK = 0x05 //5
const val IN_PLAYER_POSITION_LOOK = 0x06 //6
const val IN_BLOCK_DIGGING = 0x07 //7
const val IN_BLOCK_PLACEMENT = 0x08 //8
const val IN_ITEM_CHANGE = 0x09 //9
const val IN_ANIMATION = 0x0A //10
const val IN_ACTION = 0x0B //11
const val IN_STEER = 0x0C //12
const val IN_WINDOW_CLOSE = 0x0D //13
const val IN_WINDOW_CLICK = 0x0E //14
const val IN_WINDOW_CONFIRM = 0x0F //15
const val IN_WINDOW_SLOT = 0x10 //16
const val IN_WINDOW_ACTION = 0x11 //17
const val IN_WINDOW_TEXT = 0x12 //18
const val IN_PLAYER_FLIGHT = 0x13 //19
const val IN_TAB_COMPLETE = 0x14 //20
const val IN_SETTINGS = 0x15 //21
const val IN_STATUS = 0x16 //22
const val IN_CUSTOM = 0x17 //23
const val IN_SPECTATE = 0x18 //24
const val IN_PACK_STATUS = 0x19 //25

data class Interaction(
    val position: Position, val face: Face, val offset: Position
) { val clicked = position.relative(face) }

fun Toggled.LocationComponent(
    channel: Channel, spawn: Location
) = PublishSubject(spawn).apply {
    fun handle(id: Int, block: suspend Read.(Location) -> (Location)) =
        channel.receive(id) { invoke(block(last)) }
    handle(IN_PLAYER) { it.copy(ground = bool()) }
    handle(IN_PLAYER_POSITION) { it.copy(
        x = double(), y = double(), z = double(), ground = bool()
    ) }
    handle(IN_PLAYER_LOOK) { it.copy(
        yaw = float(), pitch = float(), ground = bool()
    ) }
    handle(IN_PLAYER_POSITION_LOOK) { Location(
        double(), double(), double(), float(), float(), bool()
    ) }
} as PublishedObservable<Location>
fun Toggled.FlagsComponent(
    player: Player, fire: PublishedObservable<Boolean>
) = PublishSubject(0.toByte()).apply {
    player.sprinting { this(if (it) last or 0x08 else last and 0x08.inv()) }
    player.sneaking { this(if (it) last or 0x02 else last and 0x02.inv()) }
    fire { this(if (it) last or 0x01 else last and 0x01.inv()) }
    player.onUse { item, _ ->
        if (item.isConsumable())
            this(last or 0x10)
    }
    player.onRelease {
        this(last and 0x10.inv())
    }
}
