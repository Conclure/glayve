package com.gitlab.glayve

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.glayve.network.Channel

typealias Streamer = Toggled.(Channel) -> (Unit)
interface Streamable {
    val streamTo: Streamer
}