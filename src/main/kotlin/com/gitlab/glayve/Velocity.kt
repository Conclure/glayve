@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.glayve

import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

inline class Velocity(private val values: DoubleArray) {
    val x get() = values[0]
    val y get() = values[1]
    val z get() = values[2]
    val compressedX get() = (x * 8000).toInt().toShort()
    val compressedY get() = (y * 8000).toInt().toShort()
    val compressedZ get() = (z * 8000).toInt().toShort()

    operator fun get(index: Int) = values[index]

    inline fun copy(x: Double = this.x, y: Double = this.y, z: Double = this.z)
            = Velocity(x, y, z)
}
inline fun Velocity(x: Double, y: Double, z: Double)
        = Velocity(doubleArrayOf(x, y, z))
inline fun Velocity(unit: Double)
        = Velocity(unit, unit, unit)
operator fun Velocity.times(amount: Double) =
    Velocity(x * amount, y * amount, z * amount)
operator fun Velocity.unaryMinus() =
    Velocity(-x, -y, -z)
operator fun Velocity.minus(other: Velocity) =
    Velocity(x - other.x, y - other.y, z - other.z)
operator fun Velocity.plus(other: Velocity) =
    Velocity(x + other.x, y + other.y, z + other.z)
operator fun Velocity.times(other: Velocity) =
    Velocity(x * other.x, y * other.y, z * other.z)
operator fun Velocity.div(other: Velocity) =
    Velocity(x / other.x, y / other.y, z / other.z)

val Velocity.lengthSquared get() = (x * x) + (y * y) + (z * z)
val Velocity.length get() = sqrt(lengthSquared)
fun Velocity.normalize() = length.let { Velocity(x / it, y / it, z / it) }



data class Bounds(val min: Velocity, val max: Velocity) {
    constructor(
        x1: Double, y1: Double, z1: Double,
        x2: Double, y2: Double, z2: Double
    ): this(
        Velocity(min(x1, x2), min(y1, y2), min(z1, z2)),
        Velocity(max(x1, x2), max(y1, y2), max(z1, z2))
    )
}
operator fun Bounds.contains(point: Velocity) =
    point.x in min.x..max.x &&
            point.y in min.y..max.y &&
            point.z in min.z..max.z
fun Bounds.intersects(other: Bounds): Boolean {
    return other.max.x > this.min.x &&
            other.min.x < this.max.x &&
            other.max.y > this.min.y &&
            other.min.y < this.max.y &&
            other.max.z > this.min.z &&
            other.min.z < this.max.z
}
fun Bounds.expand(amount: Velocity)
        = Bounds(min - amount, max + amount)