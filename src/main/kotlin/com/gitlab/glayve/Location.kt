package com.gitlab.glayve

import com.gitlab.glayve.network.Bool
import com.gitlab.glayve.network.Position
import java.lang.Math.toRadians
import kotlin.math.cos
import kotlin.math.sin

data class Location(
    val x: Double, val y: Double, val z: Double,
    val yaw: Float, val pitch: Float, val ground: Bool
)

fun Location.distance(other: Location): Double {
    val deltaX = x - other.x
    val deltaY = y - other.y
    val deltaZ = z - other.z
    return kotlin.math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)
}

fun Location.add(x: Double = 0.0, y: Double = 0.0, z: Double = 0.0)
    = copy(x = this.x + x, y = this.y + y, z = this.z + z)

operator fun Location.plus(other: Velocity) =
    copy(x + other.x, y + other.y, z + other.z)

val Location.direction get(): Velocity {
    val yRotation = toRadians(pitch.toDouble())
    val xRotation = toRadians(yaw.toDouble())
    val xz = cos(yRotation)
    return Velocity(
        -xz * sin(xRotation),
        -sin(yRotation),
        xz * cos(xRotation)
    )
}

fun Location.toPosition() = Position(x.toInt(), y.toInt(), z.toInt())
fun Location.toVelocity() = Velocity(x, y, z)

val Location.compressedYaw get() = ((yaw * 256) / 360f).toInt().toByte()
val Location.compressedPitch get() = ((pitch * 256) / 360f).toInt().toByte()