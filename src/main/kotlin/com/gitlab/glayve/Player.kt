package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.*
import java.util.*
import com.gitlab.ballysta.architecture.invoke
import com.mojang.authlib.properties.Property
import java.util.concurrent.ThreadLocalRandom.*
import kotlin.experimental.or
import kotlin.math.ceil
import kotlin.math.floor


inline class Effect(private val value: Int) {
    constructor(length: Int, strength: Int):
        this(length shl 8 or strength)

    val duration get() = (value ushr 8).ticks
    val strength get() = value and 0xFF
}

data class Transaction(val window: Byte, val action: Short)

interface Inventory : Mutable<Slot, Item> {
    val cursor: PublishedObservable<Item>

    val onPickup: Event<(Slot>
}

interface Player {
    val name: String
    val uuid: UUID
    val properties: Array<Property>

    val id: Int
    val channel: Channel

    val inventory: Inventory

    val onChat: Event<(String) -> (Unit)>
    val onInteract: Event<(Int, Velocity?) -> (Unit)>
    val location: PublishedObservable<Location>
    val onAttack: Event<(Int) -> (Unit)>
    val onBreak: Event<(Position, Face, Byte) -> (Unit)>
    val onDrop: Event<(Bool) -> (Unit)>
    val onRelease: Event<() -> (Unit)>
    val onUse: Event<(Item, Interaction?) -> (Unit)>
    val selected: PublishedObservable<Slot>
    val onSwing: Event<() -> (Unit)>
    val sneaking: PublishedObservable<Boolean>
    val sprinting: PublishedObservable<Boolean>

    val onWindowSlot: Event<(Byte, Item) -> (Unit)>
}

fun Toggled.Player(
    channel: Channel, spawn: Location, id: Int,
    health: PublishedObservable<Float>,
    food: PublishedObservable<Int>,
    gamemode: PublishedObservable<Gamemode>
) = object : Player {
    override val name = channel.profile.name
    override val uuid = channel.profile.id
    override val properties = channel.profile.properties.values().toTypedArray()
    override val id = id
    override val channel = channel

    override val inventory = object : Inventory {
        val items = Array(44) { Item.EMPTY }

        override val onChanged = TreeEvent<(Slot, Item?, Item?) -> (Unit)>()
        override val cursor = PublishSubject(Item.EMPTY)

        override val keys = items.indices.map { it.toByte() }.enumerator()
        override val values = items.enumerator()

        override fun get(key: Slot) = if (key in items.indices) null else items[key]
        override fun set(key: Slot, value: Item?) =
            if (key !in items.indices) null
            else items[key].apply {
                items[key.toInt()] = value ?: Item.EMPTY
                onChanged(key, this, value ?: Item.EMPTY)
                channel.setSlot(0, key, value ?: Item.EMPTY)
            }
    }

    override val onChat = TreeEvent<(String) -> (Unit)>()
    override val onInteract = TreeEvent<(Int, Velocity?) -> (Unit)>()
    override val location = PublishSubject(spawn)
    override val onAttack = TreeEvent<(Int) -> (Unit)>()
    override val onBreak = TreeEvent<(Position, Face, Byte) -> (Unit)>()
    override val onDrop = TreeEvent<(Bool) -> (Unit)>()
    override val onRelease = TreeEvent<() -> (Unit)>()
    override val onUse = TreeEvent<(Item, Interaction?) -> (Unit)>()
    override val selected = PublishSubject(36.toByte())
    override val onSwing = TreeEvent<() -> (Unit)>()
    override val sneaking = PublishSubject(false)
    override val sprinting = PublishSubject(false)

    override val onWindowSlot = TreeEvent<(Slot, Item) -> (Unit)>()

    override fun toString() = name

    private operator fun Array<Item>.get(slot: Slot) = get(slot.toInt())
    private operator fun Array<Item>.set(slot: Slot, value: Item?) = set(slot.toInt(), value ?: Item.EMPTY)

    init {
        health { channel.updateHealth(it, food.last, 0.0001f) }
        food { channel.updateHealth(health.last, it, 0.0001f) }
        gamemode { channel.state(3, it.ordinal.toFloat()) }

        channel.receive(IN_CHAT) {
            val bytes = ByteArray(varInt())
            for (i in bytes.indices) bytes[i] = byte()
            onChat(bytes.toString(Charsets.UTF_8))
        }
        channel.receive(IN_INTERACT) {
            val entity = varInt()
            when (varInt()) {
                0 -> onInteract(entity, null)
                1 -> onAttack(entity)
                else -> onInteract(entity, Velocity(
                    x = float().toDouble(),
                    y = float().toDouble(),
                    z = float().toDouble()
                )
                )
            }
        }
        channel.receive(IN_PLAYER) { location(location.last.copy(ground = bool())) }
        channel.receive(IN_PLAYER_POSITION) { location(location.last.copy(
            x = double(), y = double(), z = double(), ground = bool()
        )) }
        channel.receive(IN_PLAYER_LOOK) { location(location.last.copy(
            yaw = float(), pitch = float(), ground = bool()
        )) }
        channel.receive(IN_PLAYER_POSITION_LOOK) { location.invoke(Location(
            double(), double(), double(), float(), float(), bool()
        )) }
        channel.receive(IN_BLOCK_DIGGING) {
            val status = byte().toInt()
            val block = position()
            val face = face()
            when (status) {
                0 -> onBreak(block, face, 0)
                1 -> onBreak(block, face, 1)
                2 -> onBreak(block, face, 2)
                3 -> onDrop(true)
                4 -> onDrop(false)
                5 -> onRelease()
            }
        }
        channel.receive(IN_BLOCK_PLACEMENT) {
            val position = position()
            val face = byte().toInt()
            val slot = item()
            val offset = Position(byte().toInt(), byte().toInt(), byte().toInt())
            onUse(slot,
                if (face !in Face.values().indices) null
                else Interaction(position, Face.values()[face], offset)
            )
        }
        channel.receive(IN_ITEM_CHANGE) { selected((36 + short()).toByte()) }
        channel.receive(IN_ANIMATION) { onSwing() }
        channel.receive(IN_ACTION) {
            varInt() //Entity id. (Ignored)
            when (varInt()) {
                0 -> sneaking(true)
                1 -> sneaking(false)
                3 -> sprinting(true)
                4 -> sprinting(false)
            }
            varInt() //Horse jump boost. (Ignored)
        }

        channel.receive(IN_WINDOW_CLICK) {
            val window = byte()
            val slot = slot()
            val button = byte().toInt()
            val transaction = Transaction(window, short())
            val mode = byte()
            val item = item()


            if (window < 1) when (mode.toInt()) {
                0 -> {
                    //fire actual event... (with transaction)
                    val clicked = inventory.items[slot.toInt()]
                    val result = if (button != 0) clicked.copy(
                        count = (clicked.count / 2).toByte()
                    ) else clicked
                    inventory.items[slot] = result
                    inventory.onChanged(slot, clicked, result)
                    inventory.cursor(clicked.copy(
                        count = (clicked.count - result.count).toByte()
                    ))
                }
                1 -> inventory.onShiftClick(button, slot, item)
                2 -> inventory.onHotbar(slot, (36 + button).toByte(), inventory.contents[slot]!!)
                3 -> inventory.onMiddle(slot, item)
                4 -> if (slot.toInt() != -999) inventory.onDrop(slot, button, item)
                5 -> null
                6 -> inventory.onDouble(slot, item)
            }
        }

        channel.receive(IN_WINDOW_SLOT) { inventory[slot()] = item() }
    }
}

fun Player.kick(message: Any) {
    channel.send(OUT_DISCONNECT) { string("{\"text\":\"$message\"}") }
}

fun Player.abilities(
    invulnerable: Bool, flying: Bool, creative: Bool,
    flySpeed: Float, walkSpeed: Float
) {
    var flags = 0.toByte()
    if (invulnerable) flags = flags or 0x1
    if (flying) flags = flags or 0x2
    if (creative) flags = flags or 0x8
    if (flySpeed > 0) flags = flags or 0x4
    channel.send(OUT_ABILITIES) {
        byte(flags); float(flySpeed); float(walkSpeed)
    }
}



val width = 0.3
val height = 1.8
//val width = 1.0
//val height = 2.0
val Player.bounds get() = location.map {
    Bounds(
        it.x - width, it.y, it.z - width,
        it.x + width, it.y + height, it.z + width
    )
}

fun Player.send(message: Any, color: Any = "white", action: Bool = false)
    = channel.chat(message, color, action)
