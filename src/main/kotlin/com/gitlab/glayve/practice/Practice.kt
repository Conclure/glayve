package com.gitlab.glayve.practice

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.network.*
import java.nio.file.Paths

fun Toggled.Practice(players: Mutable<*, Player>) {
    val duels = MutableHolder<Pair<Player, Player>>()
    players.onEach { _, player ->
        player.onChat {
            when {
                it.startsWith("/duel") -> {
                    val name = it.split(" ").getOrNull(1)
                    val target = players.values.filter { other -> other.name.equals(name, ignoreCase = true) }()

                    if (target != null) {
                        duels.add(player to target)
                    }
                }
            }
        }
    }
    val region = LoadedWorld(chunks(Paths.get("r.0.0.mca")))
    duels.onEach { _, (first, second) ->
        first.send("Duel has started against ${second.name}.", "blue")
        second.send("Duel has started against ${first.name}.", "blue")

        val world = HashWorld(region)
        respawn(first.channel)
        respawn(second.channel)

        WorldComponent(first, world) { 0 }.streamTo(second.channel)
        WorldComponent(second, world) { 0 }.streamTo(first.channel)

      //  first.channel.spawn(second, 0, meta(second.name))
      //  second.channel.spawn(first, 0, meta(first.name))
    }

}

fun setup(inventory: MutableTable<Slot, Item>, effects: Mutable<EffectType, Effect>) {
    Effect(20 * 1000, 1).also { effects[EffectType[1]] = it }
    Effect(20 * 1000, 5).also { effects[EffectType[10]] = it }

    for (i in 0..3) inventory[(5 + i).toByte()] = Item((310 + i).toShort(), 0, 1, mutableMapOf())

    inventory[36] = Item(276, 0, 1, mutableMapOf())
    for (index in 9..44) {
        if (index != 36) {
            inventory[index.toByte()] = Item(373, 16421, 1, mutableMapOf())
        }
    }
}

fun respawn(channel: Channel) {
    channel.respawn(1, 2, 0, "test")
    channel.respawn(0, 2, 0, "test")
}

fun meta(name: String) = mapOf(
    0 to Meta(0, 0.toByte()), 1 to Meta(1, 0.toShort()),
    2 to Meta(4, name), 3 to Meta(0, 1.toByte()),
    4 to Meta(0, 0.toByte()), 6 to Meta(3, 20.toFloat()),
    7 to Meta(2, 0), 8 to Meta(0, 0.toByte()),
    9 to Meta(0, 0.toByte()), 15 to Meta(0, 0.toByte()),
    10 to Meta(0, 0.toByte()), 16 to Meta(0, 0.toByte()),
    17 to Meta(3, 0.toFloat()), 18 to Meta(2, 0)
)