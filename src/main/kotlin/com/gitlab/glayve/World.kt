package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.*
import kotlinx.coroutines.*

interface World : Streamable {
    operator fun get(position: Position): Int
}
interface MutableWorld : World {
    operator fun set(position: Position, type: Int): Int
}

fun LoadedWorld(chunks: List<Chunk>) = object :  World {
    override fun get(position: Position): Int {
        val chunk = chunks.find {
            it.x == position.x / 16 && it.z == position.z / 16
        }
        val section = chunk?.sections?.get(position.y / 16) ?: return 0
        if (section.isEmpty) return 0
        val x = position.x and 15
        val y = position.y and 15
        val z = position.z and 15
        val index = x or (z shl 4) or (y shl 8)
        return section.blockData[index].toInt()
    }

    val packets = chunks.chunked(15) { it.toTypedArray() }
    override val streamTo: Streamer = { channel ->
        onEnabled { packets.forEach { chunk -> channel.chunkBulk(chunk, sky = true) } }
    }
}
fun HashWorld(world: World) = object : MutableWorld {
    val changes = MutableTable<Position, Int>()
    override fun get(position: Position) =
        changes[position] ?: world[position]
    override fun set(position: Position, type: Int) =
        changes.set(position, type) ?: world[position]

    override val streamTo: Streamer = { channel ->
        world.streamTo(channel)
        changes.onEach { position, type ->
            channel.blockChange(position, type)
        }
    }
}

fun Toggled.WorldComponent(
    player: Player, world: MutableWorld, time: (Position) -> (Int)
): Streamable {
    //maintain the world for this player
    world.streamTo(player.channel)

    //save any broken blocks as changes.
    val watching = ArrayList<Channel>()
    var task: Job? = null
    player.onBreak { position, _, status ->
        fun pop() {
            val type = world.set(position, 0)
            val state = (type ushr 4) or ((type and 0xF) shl 12)
            watching.forEach {
                it.breakAnimation(player.id, position, -1)
                it.effect(2001, position, state, false)
            }
        }
        when (status.toInt()) {
            0 -> task = GlobalScope.launch {
                val period = time(position).ticks / 10
                if (period.isPositive()) {
                    for (stage in 0..9) {
                        watching.forEach {
                            it.breakAnimation(player.id, position, -1)
                        }
                        delay(period)
                    }
                    player.channel.sound("random.bow", player.location.last, 1f, 1f)
                } else pop()
            } 2 -> pop() else -> {
                task?.cancel("Cancelled"); task = null
                watching.forEach {
                    it.breakAnimation(player.id, position, -1)
                }
            }
        }
    }

    player.onUse { slot, interaction ->
        if (interaction != null) //TODO check if placeable type
            world[interaction.clicked] = slot.combinedType
    }

    //Set the time to noon and disable the daylight cycle.
    player.channel.timeUpdate(6000L, false)
    //spawn the player into the world
    player.channel.teleport(player.location.last)
    return object : Streamable {
        override val streamTo: Streamer = {
            onEnabled { watching += it }
            onDisabled { watching -= it }
        }
    }
}