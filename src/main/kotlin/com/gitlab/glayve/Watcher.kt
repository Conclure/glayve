package com.gitlab.glayve

import com.gitlab.ballysta.architecture.PublishedObservable
import com.gitlab.glayve.network.Item
import com.gitlab.glayve.network.item
import com.gitlab.glayve.network.string
import com.gitlab.mynt.base.Write

typealias Watchable<Type> = suspend Write.(Int, Type) -> (Unit)
fun <Type> Watchable(
    type: Int, writer: suspend Write.(Type) -> (Unit)
): Watchable<Type> = { id, value ->
    byte((type shl 5 or id).toByte()); writer(value)
}

val BYTE = Watchable<Byte>(0) { byte(it) }
val SHORT = Watchable<Short>(1) { short(it) }
val INT = Watchable<Int>(2) { int(it) }
val FLOAT = Watchable<Float>(3) { float(it) }
val STRING = Watchable<String>(4) { string(it) }
val ITEM = Watchable<Item>(5) { item(it) }

class Watcher<Type>(val type: Watchable<Type>, val index: Int, val value: PublishedObservable<Type>)

/*
    //TODO figure out if the ordering here matters that much.
    metadata.forEach {
        (it.value as Observable<Any>) { value ->
            viewer.send(OUT_METADATA) {
                varInt(id)
                (it.type as Watchable<Any>)(this, it.index, value)
                byte(127.toByte())
            }
        }
    }
 */