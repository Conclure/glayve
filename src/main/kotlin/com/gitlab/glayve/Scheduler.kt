package com.gitlab.glayve

import com.gitlab.ballysta.architecture.Event
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit.*
import kotlin.time.Duration
import kotlin.time.milliseconds


typealias Scheduler = ScheduledExecutorService

val Int.ticks get() = times(50).milliseconds
val Long.ticks get() = times(50L).milliseconds
val Duration.inTicks get() = inMilliseconds / 50
fun Duration.toLongTicks() = toLongMilliseconds() / 50L

val tick = 1.ticks

fun Scheduler.every(duration: Duration): Event<(Int) -> (Unit)> = {
    var task: ScheduledFuture<*>? = null; var times = 0
    //TODO consider the implications of this.
    onDisabled { task?.cancel(true); times = 0 }
    onEnabled { task = scheduleAtFixedRate({
        if (enabled) it(times++)
    }, 0L, duration.toLong(MILLISECONDS), MILLISECONDS) }
}
fun Scheduler.after(duration: Duration): Event<() -> (Unit)> = {
    var task: ScheduledFuture<*>? = null
    onDisabled { task?.cancel(true) }
    onEnabled { task = schedule(
        { if (enabled) it() },
        duration.toLong(MILLISECONDS),
        MILLISECONDS
    ) }
}
