package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.*
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import kotlin.experimental.and
import kotlin.experimental.or

fun Toggled.EntityPlayer(
    viewer: Channel, id: Int, uuid: UUID,

    location: PublishedObservable<Location>,
    health: PublishedObservable<Float>,

    hand: PublishedObservable<Item?>,
    head: PublishedObservable<Item?>,
    chest: PublishedObservable<Item?>,
    legs: PublishedObservable<Item?>,
    feet: PublishedObservable<Item?>,

    //todo replace with watcher
    sprinting: PublishedObservable<Boolean>,
    sneaking: PublishedObservable<Boolean>,
    burning: PublishedObservable<Boolean>,
    using: PublishedObservable<Boolean>,

    onSwing: Event<() -> (Unit)>
) {
    val flags = PublishSubject(0.toByte()).apply {
        using { this(if (it) last or 0x10 else last and 0x10.inv()) }
        sprinting { this(if (it) last or 0x08 else last and 0x08.inv()) }
        sneaking { this(if (it) last or 0x02 else last and 0x02.inv()) }
        burning { this(if (it) last or 0x01 else last and 0x01.inv()) }
    }
    val meta = mapOf(
        0 to Meta(0, flags.last), 1 to Meta(1, 0.toShort()),
        2 to Meta(4, ""), 3 to Meta(0, 1.toByte()),
        4 to Meta(0, 0.toByte()), 6 to Meta(3, 20.toFloat()),
        7 to Meta(2, 0), 8 to Meta(0, 0.toByte()),
        9 to Meta(0, 0.toByte()), 15 to Meta(0, 0.toByte()),
        10 to Meta(0, 0.toByte()), 16 to Meta(0, 0.toByte()),
        17 to Meta(3, 0.toFloat()), 18 to Meta(2, 0)
    )
    onEnabled { viewer.spawn(id, uuid, location.last, hand.last?.type ?: 0, meta) }
    hand { viewer.equipment(id, 0, it ?: Item.EMPTY) }
    head { viewer.equipment(id, 4, it ?: Item.EMPTY) }
    chest { viewer.equipment(id, 3, it ?: Item.EMPTY) }
    legs { viewer.equipment(id, 2, it ?: Item.EMPTY) }
    feet { viewer.equipment(id, 1, it ?: Item.EMPTY) }
    flags { viewer.metadata(id, mapOf(0 to Meta(0, it))) }
    location.onNext { from, to ->
        viewer.entityMove(id, from, to)
        viewer.headMove(id, to.yaw)
    }
    health.onNext { from, to -> if (to < from) {
        val pitch = (ThreadLocalRandom.current().nextFloat() - ThreadLocalRandom.current().nextFloat()) * 0.2f + 1.0f
        viewer.sound("game.neutral.hurt", location.last, 1f, pitch)
        viewer.animation(id, 1)
    } }
    onSwing { viewer.animation(id, 0) }
    onDisabled { viewer.despawn(id) }
}