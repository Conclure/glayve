package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.despawn
import com.gitlab.glayve.network.spawnObject
import com.gitlab.glayve.network.state
import com.gitlab.glayve.network.updateHealth
import java.time.Duration.between
import java.time.Instant
import java.time.Instant.now
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.math.min

fun Toggled.ThrowableComponent(
    player: Player,
    players: Mutated<*, Player>,
    scheduler: Scheduler,
    world: World,
    health: PublishedObservable<Float>
) {
    var start: Instant? = null
    player.onUse { item, interaction ->
        if (item.type == 261.toShort()) start = now()
        else if (item.type == 373.toShort() && interaction == null) {
            val force = splashVelocity(player.location.last)
            val origin = player.location.last.add(y = 1.52)
            EntityPotion(scheduler, eid++, item.meta, world, players, origin, force) { hit, power ->
                if (health.last > 14f) {
                    hit.channel.updateHealth(20f, 20, 5f)
                } else {
                    hit.channel.updateHealth(health.last + 6f, 20, 5f)
                }
               // val i = (power.toDouble() * (4 shl 1).toDouble() + 0.5).toInt()
               // println("healed for ${i.toFloat()}")
            //    hit.channel.updateHealth(health.last + i, 20, 5f)
                //println("Hit: $hit")
            }.enable()
        }
    }
    player.onRelease {
        if (start != null) {
            val ticks = between(start, now()).toMillis() / 50
            val i = (ticks / 20.0).toFloat()
            val f = (i * i + i * 2.0f) / 3.0f
            start = null
            if (f >= 0.1) Component {
                val id = eid++
                val force = arrowVelocity(player.location.last, min(1f, f) * 2)
                val origin = player.location.last.add(y = 1.52)
                val velocity = ProjectileVelocity(scheduler, force, 0.05)
                val location = ProjectileLocation(origin, velocity)
                location { to ->
                    val from = location.last.toVelocity()
                    val slope = to.toVelocity() - from
                    for (other in players.values.filter { it !== player }) {
                        val bounds = other.bounds.last
                        val enter = intersection(bounds, from, slope.normalize())
                        if (enter * enter <= slope.lengthSquared) {
                            player.channel.state(6, 0f)
                            disable(); return@location
                        }
                    }
                    for (block in location.last..to)
                        if (world[block].isSolid()) {
                            disable(); return@location
                        }
                    if (to.y < 0) disable()
                }
                onEnabled {
                    players.send { spawnObject(id, 60, location.last, player.id, velocity.last) }
                    val pitch = 1.0f / (current().nextFloat() * 0.4f + 1.2f) + f * 0.5f
                    players.send("random.bow", player.location.last, 1.0f, pitch)
                }
                onDisabled {
                    val pitch = 1.2f / (current().nextFloat() * 0.2f + 0.9f)
                    players.send { despawn(id) }
                    players.send("random.bowhit", location.last, 1f, pitch)
                }
            }.enable()
        }


        /*player.onUse { item, _ ->
            if (item.type == 373.toShort()) {
                val force = splashVelocity(player.location.last)
                val origin = player.location.last.add(y = 1.52)
                EntityPotion(scheduler, eid++, item.meta, world, players, origin, force) { hit, _ ->
                    println("Hit: $hit")
                }.enable()
            }
        }*/
    }
}