package com.gitlab.glayve

import com.gitlab.glayve.network.*
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.nio.file.Files
import java.nio.file.Path
import java.util.zip.InflaterInputStream

interface Section {
    val isEmpty: Bool

    val blockData: ShortArray
    val emittedLight: ByteArray
    val skyLight: ByteArray
}
interface Chunk {
    val x: Int
    val z: Int
    val sections: Array<Section>
    val biomes: ByteArray
}

suspend fun loadChunk(region: Path, x: Int, z: Int): Chunk? = Files.newByteChannel(region).use {
    val read = it.toRead()
    it.position((4 * ((z and 0x1F) shl 5 or (x and 0x1F))).toLong())
    val offset = read.int() ushr 8
    if (offset != 0) {
        it.position(offset * 4096L)
        val length = read.int()
        if (read.byte() == 2.toByte()) {
            val data = ByteArray(length)
            read.bytes(data, data.size, 0)
            DataInputStream(InflaterInputStream(ByteArrayInputStream(data))).use { decompressed ->
                val level = decompressed.toRead().compound()["Level"] as TagCompound
                object : Chunk {
                    override val x = level["xPos"] as Int
                    override val z = level["zPos"] as Int
                    override val biomes = level["Biomes"] as ByteArray
                    override val sections = Array<Section>(16) { _ ->
                        object : Section {
                            override val isEmpty = true
                            override val blockData = ShortArray(0)
                            override val emittedLight = ByteArray(0)
                            override val skyLight = ByteArray(0)
                        }
                    }
                }.apply {
                    (level["Sections"] as TagList).forEach { nbt ->
                        val section = nbt as TagCompound
                        val meta = section["Data"] as ByteArray
                        val blocks = section["Blocks"] as ByteArray
                        sections[(section["Y"] as Byte).toInt()] = object : Section {
                            override val isEmpty = false
                            override val emittedLight = section["BlockLight"] as ByteArray
                            override val skyLight = section["SkyLight"] as ByteArray
                            override val blockData = ShortArray(4096) { i ->
                                (blocks[i].toInt() shl 4 or (meta[i / 2].toInt() shr 4 * (i % 2) and 0x0F)).toShort()
                            }
                        }
                    }
                }
            }
        } else throw IllegalStateException("Corrupt Region.")
    } else null
}