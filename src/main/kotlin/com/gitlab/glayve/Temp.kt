package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.*
import java.util.concurrent.ThreadLocalRandom.*
import kotlin.math.*

operator fun Location.rangeTo(to: Location): Iterable<Position> {
    val distance = (distance(to) * 10).toInt()
    val dx = (to.x - x) / distance
    val dy = (to.y - y) / distance
    val dz = (to.z - z) / distance
    val blocks = HashSet<Position>()
    for (i in 0..distance) {
        blocks += copy(
            x = x + (dx * i),
            y = y + (dy * i),
            z = z + (dz * i)
        ).toPosition()
    }
    return blocks
}

data class Ray(val from: Velocity, val to: Velocity) {
    val direction = (to - from).normalize()
    val inverse = Velocity(1.0) / direction
}

fun intersection(bounds: Bounds, from: Velocity, direction: Velocity): Double {
    val min = (bounds.min - from) / direction
    val max = (bounds.max - from) / direction
    val enter = max(max(min(min.x, max.x), min(min.y, max.y)), min(min.z, max.z))
    val exit = min(min(max(min.x, max.x), max(min.y, max.y)), max(min.z, max.z))
    return if (exit < 0 || enter > exit) Double.POSITIVE_INFINITY else enter
}

fun Ray.intersects(min: Velocity, max: Velocity): Velocity? {
    var enter = Double.NEGATIVE_INFINITY
    var exit = Double.POSITIVE_INFINITY
    for (i in 0..2) {
        if (inverse[i] == 0.0) println("parallel rip")
        val t0 = (min[i] - from[i]) * inverse[i]
        val t1 = (max[i] - from[i]) * inverse[i]
        val tMin = min(t0, t1)
        val tMax = max(t0, t1)
        if (tMin > enter) enter = tMin
        if (tMax < exit) exit = tMax
        if (enter > exit || exit < 0)
            return null
        if (exit < max(0.0, enter) || enter >= Double.POSITIVE_INFINITY)
            return null
    }
    if (enter > (to - from).length) return null
    return from + (direction * enter)
}


fun Double.toRadians() = this / 180 * PI
fun Float.toRadians() = this / 180 * PI
fun Double.toDegrees() = this * 180 / PI
fun Float.toDegrees() = this * 180 / PI

fun Mutated<*, Player>.send(block: Channel.() -> (Unit)) = values.map(Player::channel).forEach(block)
fun Mutated<*, Player>.send(text: String, color: String = "white", action: Boolean = false) =
    send { chat(text, color, action) }

fun Mutated<*, Player>.send(name: String, location: Location, volume: Float, pitch: Float) {
    values.forEach {
        val range = if (volume > 1.0f) (16.0f * volume).toDouble() else 16.0
        if (it.location.last.distance(location) < range)
            it.channel.sound(name, location, volume, pitch)
    }
}

infix fun ClosedRange<Double>.step(step: Double): Iterable<Double> {
    require(start.isFinite())
    require(endInclusive.isFinite())
    require(step > 0.0) { "Step must be positive, was: $step." }
    val sequence = generateSequence(start) { previous ->
        if (previous == Double.POSITIVE_INFINITY) return@generateSequence null
        val next = previous + step
        if (next > endInclusive) null else next
    }
    return sequence.asIterable()
}

fun Toggled.Trace(channel: Channel, ray: PublishedObservable<Ray>) {
    val count = 100
    ray { (from, to) ->
        val dx = (to.x - from.x) / count
        val dy = (to.y - from.y) / count
        val dz = (to.z - from.z) / count
        for (i in 0..count) {
            val x = (from.x + (dx * i)).toFloat()
            val y = (from.y + (dy * i)).toFloat()
            val z = (from.z + (dz * i)).toFloat()
            channel.particle(30, true, x, y, z, 0f, 0f, 0f, 0f, 1)
        }
    }
}

fun Toggled.Outline(channel: Channel, bounds: PublishedObservable<Bounds>) {
    bounds {
        fun particle(x: Double, y: Double, z: Double) {
            channel.particle(
                30, true,
                x.toFloat(), y.toFloat(), z.toFloat(),
                0f, 0f, 0f, 0f, 1
            )
        }

        val count = 0.3
        for (x in (it.min.x..it.max.x) step count) {
            particle(x, it.max.y, it.max.z)
            particle(x, it.min.y, it.min.z)
            particle(x, it.min.y, it.max.z)
            particle(x, it.max.y, it.min.z)
        }
        for (z in (it.min.z..it.max.z) step count) {
            particle(it.max.x, it.max.y, z)
            particle(it.min.x, it.min.y, z)
            particle(it.min.x, it.max.y, z)
            particle(it.max.x, it.min.y, z)
        }
        for (y in (it.min.y..it.max.y) step count) {
            particle(it.max.x, y, it.max.z)
            particle(it.min.x, y, it.min.z)
            particle(it.min.x, y, it.max.z)
            particle(it.max.x, y, it.min.z)
        }
    }
}

fun Int.isSolid() = this != 0 && this != 4176
fun Item.isConsumable() = when (type.toInt()) {
    276, 283, 267, 272, 268, 261, 364 -> true
    else -> false
}

fun Item.damage() = when (type.toInt()) {
    283, 268 -> 4f
    272 -> 5f
    267 -> 6f
    276 -> 2f // normal is 7
    else -> 1f
}

enum class Gamemode { SURVIVAL, CREATIVE, ADVENTURE, SPECTATOR }


typealias Color = Int

val Color.red get() = this shr 16 and 255
val Color.green get() = this shr 8 and 255
val Color.blue get() = this shr 0 and 255
inline operator fun Color.component1() = red
inline operator fun Color.component2() = green
inline operator fun Color.component3() = blue
fun Color(red: Int, green: Int, blue: Int) =
    (red shl 16) or (green shl 8) or blue

var FRICTION = 2.0
var HORIZONTAL = 0.35
var VERTICAL = 0.35
var EXTRA_VERTICAL = 0.085
var EXTRA_HORIZONTAL = 0.35

fun Toggled.Knockback(players: Mutable<*, Player>) {
    players.onEach { _, player ->
        player.onChat {
            val args = it.split(" ")
            val amount = args.getOrNull(1)?.toDoubleOrNull()

            if (it.startsWith("/kb")) {
                player.send("Current KB:", "red")
                player.send("Horizontal: $HORIZONTAL", "blue")
                player.send("Vertical: $VERTICAL", "blue")
                player.send("Extra-Ver: $EXTRA_VERTICAL", "blue")
                player.send("Extra-Hor: $EXTRA_HORIZONTAL", "blue")
                player.send("Friction: $FRICTION", "blue")
            } else {
                if (amount != null) {
                    when {
                        it.startsWith("/hor") -> HORIZONTAL = amount
                        it.startsWith("/ver") -> VERTICAL = amount
                        it.startsWith("/friction") -> FRICTION = amount
                        it.startsWith("/extraver") -> EXTRA_VERTICAL = amount
                        it.startsWith("/extrahor") -> EXTRA_HORIZONTAL = amount
                    }
                }
            }
        }
    }
}

fun knockbackVelocity(location: Location, attacker: Player?, first: Boolean, test: Boolean) = if (attacker != null) {
    val from = attacker.location.last
    var x = location.x - from.x
    var z = location.z - from.z
    var y = 0.0
    while (x * x + z * z < 1.0E-4) current().apply {
        x = (nextDouble() - nextDouble()) * 0.01
        z = (nextDouble() - nextDouble()) * 0.01
    }

    val distance = sqrt(x * x + z * z)
    val x1 = x / distance
    val z1 = z / distance

    x /= FRICTION
    y /= FRICTION
    z /= FRICTION
    x -= x1 * HORIZONTAL
    y += VERTICAL
    z -= z1 * HORIZONTAL

    if (attacker.sprinting.last && first && test) {
        val yaw = from.yaw * PI
        x += -sin(yaw / 180) * EXTRA_HORIZONTAL
        z += cos(yaw / 180) * EXTRA_HORIZONTAL
        y += EXTRA_VERTICAL; x *= 0.6; z *= 0.6
    }; Velocity(x, y, z)
} else Velocity(0.0, -0.078375, 0.0)

fun knockbackVelocity(location: Location, attacker: Player?) = if (attacker != null) {
    val from = attacker.location.last
    var x = location.x - from.x
    var z = location.z - from.z
    var y = 0.36075
    while (x * x + z * z < 1.0E-4) current().apply {
        x = (nextDouble() - nextDouble()) * 0.01
        z = (nextDouble() - nextDouble()) * 0.01
    }
    val distance = sqrt(x * x + z * z)
    x /= distance; x *= 0.40
    z /= distance; z *= 0.40
    if (attacker.sprinting.last) {
        val yaw = from.yaw * PI
        x += -sin(yaw / 180) * 1.5
        z += cos(yaw / 180) * 1.5
        y += 0.1; x *= 0.6; z *= 0.6
    }; Velocity(x, y, z)
} else Velocity(0.0, -0.078375, 0.0)


fun Toggled.InventoryComponent(player: Player, inventory: Mutable<Slot, Item>, world: World, players: Mutable<*, Player>) {
    val drag = PublishSubject(0)
    val dragSlots = MutableHolder<Short>()
    var holding = Item(0, 0, 0, emptyMap())

    player.channel.receive(IN_WINDOW_CLICK) {
        val window = byte().toInt()
        val slot = short().toByte()
        val button = byte().toInt()
        val action = short()
        val mode = byte().toInt()
        val item = item()


        drag(button and 3)

        when (mode) {
            0 -> {
                if (holding.type.toInt() == 0 && item.type > 0) {
                    holding = item
                    inventory[slot] = Item(0, 0, 0, emptyMap())
                } else if (holding.type > 0) {
                    val clicked = inventory[slot]

                    if (clicked == null) {
                        inventory[slot] = holding
                    } else if (clicked.type.toInt() == 0 || clicked.type == holding.type && clicked.count + holding.count <= 64) {
                        inventory[slot] = holding.copy(count = (holding.count + clicked.count).toByte())
                    }

                    holding = Item(0, 0, 0, emptyMap())
                }
            }


            1 -> {
                if (item.type.toInt() == 0) {
                    drag(0)
                } else {
                    when (drag.last) {
                        0 -> drag(button shr 2 and 3)
                        1 -> if (item.type.toInt() == 0) dragSlots.add(slot.toShort())
                        2 -> {
                            if (dragSlots.size != 0) {
                                val count = item.count.toInt()

                                dragSlots.forEach { id, _ ->
                                    if (inventory[id.toByte()]!!.type.toInt() == 0) {

                                    }
                                }
                            }
                        }
                    }
                }
            }

            2 -> {
                val found = inventory[slot]

                if (found != null) {
                    val index = (36 + button).toByte()
                    val replace = inventory[index]
                    inventory[index] = found
                    inventory[slot] = if (replace != null && replace.type > 0) replace else Item(0, 0, 0, emptyMap())
                }
            }

            4 -> {
                val found = inventory[slot]

                if (found != null) {
                    val size = found.count.toInt()

                    if (found.type > 0) {
                        val itemId = eid++
                        val force = dropVelocity(player.location.last)
                        val origin = player.location.last.add(y = 1.3199999928474426)

                        val count = if (button == 1) size.toByte() else 1
                        EntityDrop(scheduler, itemId, world, players, origin, force, found.copy(count = count)).enable()
                    }

                    if (size == 1) {
                        inventory[player.selected.last] = Item(0, 0, 0, emptyMap())
                    } else if (size != 0) {
                        inventory[slot] = found.copy(count = (size - 1).toByte())
                    }
                }
            }

            6 -> {
                if (holding.type > 0) {
                    var size = holding.count.toInt()

                    for (i in 9..44) {
                        if (size < 64) {
                            val found = inventory[i.toByte()]
                            if (found != null && found.type == holding.type && holding.count + found.count <= 64) {
                                size += found.count
                                // need to update the item they are now clicked onto
                                inventory[slot] = Item(0, 0, 0, emptyMap())
                                inventory[i.toByte()] = Item(0, 0, 0, emptyMap())
                            }
                        }
                    }

                    println(size)
                    holding = holding.copy(count = size.toByte())
                }
            }
        }
    }
}