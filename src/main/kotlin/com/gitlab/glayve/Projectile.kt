package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.*
import jdk.nashorn.internal.objects.NativeArray.every
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.math.*

val SPLASH = 0.8f //1.52
val ARROW = 0.6f //1.899999999

fun arrowVelocity(location: Location, draw: Float): Velocity {
    val yaw = location.yaw / 180 * PI
    val pitch = location.pitch / 180 * PI
    var x = -sin(yaw) * cos(pitch)
    var y = -sin(pitch)
    var z = cos(yaw) * cos(pitch)
    val dist = sqrt(x * x + y * y + z * z)
    x /= dist
    y /= dist
    z /= dist
    fun rand(): Double {
        val direction = if (current().nextBoolean()) -1 else 1
        val random = current().nextGaussian() * direction
        return random * 0.007499999832361937
    }
    x += rand()
    y += rand()
    z += rand()
    val velocity = draw * 1.5
    x *= velocity
    y *= velocity
    z *= velocity
    return Velocity(x, y, z)
}
fun splashVelocity(location: Location) : Velocity {
    val yaw = location.yaw.toRadians()
    val pitch = location.pitch.toRadians()
    val inaccuracy = -20.0
    val velocity = 0.5
    var x = -sin(yaw) * cos(pitch) * 0.4
    var y = -sin((location.pitch + inaccuracy).toRadians()) * 0.4
    var z = cos(yaw) * cos(pitch) * 0.4

    fun rand() = current().nextGaussian() * 0.007499999832361937 * 1f

    val dist = sqrt(x * x + y * y + z * z)
    x /= dist
    y /= dist
    z /= dist

    x += rand()
    y += rand()
    z += rand()

    x *= velocity
    y *= velocity
    z *= velocity

    return Velocity(x, y, z)
}
fun dropVelocity(location: Location) : Velocity {
    val yaw = location.yaw.toRadians()
    val pitch = location.pitch.toRadians()

    var x = (-sin(yaw) * cos(pitch) * 0.3f)
    var y = (-sin(pitch) * 0.3f) + 0.1f //fixed something here
    var z = (cos(yaw) * cos(pitch) * 0.3f)

    current().apply {
        val f3 = nextFloat() * PI * 2.0f
        val f2 = 0.02f * nextFloat()
        x += cos(f3) * f2
        y += (nextFloat() - nextFloat()) * 0.1f
        z += sin(f3) * f2
    }

    return Velocity(x, y, z)
}

//TODO perhaps merge these up and make a projectile interface?
fun Toggled.ProjectileVelocity(
    scheduler: Scheduler, velocity: Velocity,
    gravity: Double, drag: () -> (Double) = { 0.99 }
) = PublishSubject(velocity).apply {
    scheduler.every(tick)() {
        val current = drag()
        this(last.copy(
            x = last.x * current,
            y = (last.y * current) - gravity,
            z = last.z * current
        ))
    }
}
fun Toggled.ProjectileLocation(
    location: Location, velocity: PublishedObservable<Velocity>
) = PublishSubject(location).apply {
    velocity {
        val horizontal = sqrt(it.x * it.x + it.z * it.z)
        this(last.copy(
            x = last.x + it.x,
            y = last.y + it.y,
            z = last.z + it.z,
            yaw = atan2(it.x, it.z).toDegrees().toFloat(),
            pitch = atan2(it.x, horizontal).toDegrees().toFloat()
        ))
    }
    onEnabled { this(location) }
}

fun EntityDrop(
    scheduler: Scheduler, id: Int,
    world: World, players: Mutated<*, Player>,
    origin: Location, force: Velocity, item: Item
) = Component {
    val velocity = ProjectileVelocity(scheduler, force, 0.03999999910593033)
    val location = ProjectileLocation(origin, velocity)
    onEnabled {
        players.send {
            spawnObject(id, 2, location.last, 1, velocity.last)
            metadata(id, mapOf(10 to Meta(5, item)))
        }
    }
    var landed: Location? = null
    location.filter { landed == null }() { to ->
        if (to.y < 0) disable() else
        if (world[to.toPosition()].isSolid())
            if (landed == null) landed = to
    }

    scheduler.every(tick)() {
        if (it >= 10) for (player in players.values) {
            val current = landed ?: location.last
            // .425 is normal
            if (player.location.last.distance(current) <= 1) {
                val pitch = (current().run { nextFloat() - nextFloat() } * 0.7f + 1.0f)
                players.send {
                    collect(id, player.id)
                    sound("random.pop", current, 0.2f, pitch * 2f)
                }; disable();  break
            }
        }
    }


    velocity.filter {
        landed == null && (it.length < 0.00001 || (it + -velocity.last).length > 0.02)
    }() { players.send { velocity(id, it)} }

    onDisabled { players.send { despawn(id);   } }
}

fun EntityPotion(
    scheduler: Scheduler, id: Int, potion: Short,
    world: World, players: Mutated<*, Player>,
    origin: Location, force: Velocity,
    onHit: (Player, Int) -> (Unit)
) = Component {
    val velocity = ProjectileVelocity(scheduler, force, 0.05)
    val location = ProjectileLocation(origin, velocity)
    onEnabled {
        players.send { spawnObject(id, 73, location.last, potion.toInt(), velocity.last) }
        players.send("random.bow", location.last.add(y = 1.0), 0.5f, 0.5f)
    }
    location { to ->
        if (to.y < 0) disable() else
        for (block in location.last..to)
            if (world[block].isSolid()) { disable(); return@location }
    }
    velocity.filter {
        it.length < 0.00001 || (it + -velocity.last).length > 0.02
    }() { players.send { velocity(id, it) } }
    onDisabled {
        players.send {
            effect(2002, location.last.toPosition(), potion.toInt(), false)
            despawn(id)
        }
        val bounds = location.last.run { Bounds(
            x - 0.125, y, z - 0.125,
            x + 0.125, y + 0.25, z + 0.125
        ) }.expand(Velocity(4.0, 2.0, 4.0))
        players.values.filter {
            bounds.intersects(it.bounds.last)
        }.forEach {
            val distance = it.location.last.distance(location.last)


            if (distance < 4) {
                val power = (((1.0 - distance / 4.0) * 200) + 0.5).toInt()
                if (power > 20) onHit(it, power)
            }
        }
    }
}
