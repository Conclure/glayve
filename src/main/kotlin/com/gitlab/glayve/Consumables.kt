package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.sound
import com.gitlab.glayve.network.status
import java.time.Instant
import java.time.Instant.*
import java.util.concurrent.ThreadLocalRandom.*
import kotlin.time.milliseconds

val TIMES = ShortArray(65536).also {
    it[364] = 32 //Steak
    it[260] = 32 //Apple

}

fun Toggled.ConsumableComponent(player: Player, players: Mutable<*, Player>, scheduler: Scheduler) {
    val end = MutableSingle<Instant>(null)

    player.onUse { item, interaction ->
        val time = TIMES[item.type.toInt()]
        if (time > 0 && interaction == null && end.get() == null)
            end.set(now().plusMillis(50L * time))
    }


    end.onEach { _, it ->
        scheduler.every(1.ticks)() { tick ->

            if (tick % 4 == 0) {
                val volume = 0.5f + 0.5f * (current().nextInt(2))
                val pitch = (current().nextFloat() - current().nextFloat()) * 0.2f + 1f
                players.send { if (this !== player.channel) sound("random.eat", player.location.last, volume, pitch) }
            }

            if (it.isBefore(now())) {
                end.set(null)
                players.send {
                    status(if (this == player.channel) 0 else player.id, 9)
                    if (this !== player.channel)
                        sound("random.burp", player.location.last, 0.5f, current().nextFloat() * 0.1f + 0.9f)
                }
            }
        }
    }
    player.onRelease { end.set(null) }
}