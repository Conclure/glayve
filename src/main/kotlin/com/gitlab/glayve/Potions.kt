package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.EffectType.*
import com.gitlab.glayve.network.*

enum class EffectType(val color: Color) {
    SPEED_UP(8171462), SPEED_DOWN(5926017), DIG_UP(14270531), DIG_DOWN(4866583), STRENGTH(9643043),
    INSTANT_HEAL(16262179), INSTANT_HARM(4393481), JUMP_UP(2293580), CONFUSION(5578058),
    REGENERATION(13458603), ALL_DAMAGE_DOWN(10044730), FIRE_DAMAGE_DOWN(14981690), BREATHING(3035801),
    INVISIBILITY(8356754), VISION_DOWN(2039587), VISION_UP(2039713), HUNGER_UP(5797459), ALL_DAMAGE_UP(4738376),
    POISON(5149489), WITHER(3484199), RED_HEARTS(16284963), GOLD_HEARTS(2445989), HUNGER_DOWN(16262179);
    companion object {
        operator fun get(id: Int) = values()[id - 1]
    }
}

private const val BASE_SPEED = .10000000149011612
fun Toggled.Speed(attributes: Mutable<String, Double>, amplifier: Int) {
    val increased = BASE_SPEED * (1 + 0.20000000298023224 * amplifier)
    onEnabled { attributes["generic.movementSpeed"] = increased }
    onDisabled { attributes["generic.movementSpeed"] = BASE_SPEED }
}
fun Toggled.Slowness(attributes: Mutable<String, Double>, amplifier: Int) {
    val increased = BASE_SPEED * (1 + -0.15000000596046448 * amplifier)
    onEnabled { attributes["generic.movementSpeed"] = increased }
    onDisabled { attributes["generic.movementSpeed"] = BASE_SPEED }
}
fun Toggled.Regeneration(scheduler: Scheduler, health: PublishSubject<Float>, strength: Int) =
    scheduler.every((50 shr strength).ticks)() { if (health.last < 20f) health(health.last + 1f) }
fun Toggled.Poison(scheduler: Scheduler, health: PublishSubject<Float>, strength: Int) =
    scheduler.every((25 shr strength).ticks)() { if (health.last > 1f) health(health.last - 1f) }
fun Toggled.Wither(scheduler: Scheduler, health: PublishSubject<Float>, strength: Int) =
    scheduler.every((40 shr strength).ticks)() { health(health.last - 1f) }



fun Toggled.EffectsComponent(
    player: Player,
    players: Mutated<*, Player>,
    health: PublishSubject<Float>
) = MutableTable<EffectType, Effect>().apply {
    val attributes = MutableTable<String, Double>()
    onEach { type, effect ->
        when (type) {
            SPEED_UP -> Speed(attributes, effect.strength)
            SPEED_DOWN -> Slowness(attributes, effect.strength)
            REGENERATION -> Regeneration(scheduler, health, effect.strength)
            POISON -> Poison(scheduler, health, effect.strength)
            WITHER -> Wither(scheduler, health, effect.strength)
            else -> println("$type not implemented")
        }
        scheduler.after(effect.duration)() { this@apply[type] = null }
        onEnabled { players.send { addEffect(if (equals(player.channel)) 0 else player.id, type, effect) } }
        onDisabled { players.send { removeEffect(if (equals(player.channel)) 0 else player.id, type) } }
    }
    onChanged { _, _, _ ->
        var reds = 0f; var greens = 0f
        var blues = 0f; var total = 0
        forEach { type, effect ->
            val (red, green, blue) = type.color
            total += (effect.strength + 1).also {
                reds += red.toFloat() / 255 * it
                greens += green.toFloat() / 255 * it
                blues += blue.toFloat() / 255 * it
            }
        }
        val meta = mapOf(7 to Meta(2, Color(
            red = ((reds / total) * 255).toInt(),
            green = ((greens / total) * 255).toInt(),
            blue = ((blues / total) * 255).toInt()
        )))
        players.send { metadata(if (equals(player.channel)) 0 else player.id, meta) }
    }
    attributes.onChanged { name, _, to ->
        val attribute = Attribute(name, to ?: 0.0)
        players.send { attributes(if (equals(player.channel)) 0 else player.id, attribute) }
    }
}